package cat.dam.edgar.pes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CalcImc extends AppCompatActivity {
    private TextView tv_imc, tv_groupValue;
    private Button btn_backHome;
    private double weight, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc_imc);

        setVariables();
        btnListener();
        showIMC();
    }

    private void setVariables()
    {
        tv_imc = (TextView) findViewById(R.id.tv_imc);
        tv_groupValue = (TextView) findViewById(R.id.tv_groupValue);
        btn_backHome = (Button) findViewById(R.id.btn_backHome);
        setBundleVariables();
    }

    private void btnListener()
    {
        btn_backHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CalcImc.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setBundleVariables()
    {
        Bundle extras = this.getIntent().getExtras();
        weight = extras.getDouble("weight");
        height = extras.getDouble("height");
    }

    private void showIMC()
    {
        Resources res = getResources();
        double imc = calcIMC();
        String group;

        if (imc < 18.5) group = res.getString(R.string.insufficient);
        else if (imc < 25) group = res.getString(R.string.normal);
        else if (imc < 27) group = res.getString(R.string.grade_1);
        else if (imc < 30) group = res.getString(R.string.grade_2);
        else if (imc < 35) group = res.getString(R.string.type_1);
        else if (imc < 40) group = res.getString(R.string.type_2);
        else if (imc < 50) group = res.getString(R.string.type_3);
        else group = res.getString(R.string.type_4);

        setTexts(imc, group);
    }

    private double calcIMC()
    {
        double heightTemp = Math.pow(height, 2);

        return weight/heightTemp;
    }

    private void setTexts(double imc, String group)
    {
        tv_imc.setText(getIMCText(imc));
        tv_groupValue.setText(group);
    }

    private String getIMCText(double imc)
    {
        Resources res = getResources();
        String imc_1, imc_2, imc_3, finalText;
        imc = (double) Math.round(imc * 100) / 100; // Round up to 2 decimals

        imc_1 = res.getString(R.string.imc_text_1);
        imc_2 = res.getString(R.string.imc_text_2);
        imc_3 = res.getString(R.string.imc_text_3);

        finalText = imc_1 + " " + weight + " " + imc_2 + "\n" + height + " " + imc_3 + " " + imc;

        return finalText;
    }
}