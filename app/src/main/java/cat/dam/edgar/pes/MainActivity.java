package cat.dam.edgar.pes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity
{
    private EditText et_weight, et_height;
    Button btn_calc;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        setListeners();
    }

    private void setVariables()
    {
        et_weight = (EditText) findViewById(R.id.et_weight);
        et_height = (EditText) findViewById(R.id.et_height);
        btn_calc = (Button) findViewById(R.id.btn_calc);
    }

    private void setListeners()
    {
        btn_calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                double weight, height;

                weight = getEtValues("weight");
                height = getEtValues("height");

                startCalc(weight, height);
            }
        });
    }

    private double getEtValues(String value)
    {
        if(value.equals("weight")) return Double.parseDouble(et_weight.getText().toString());

        return Double.parseDouble(et_height.getText().toString());
    }

    private void startCalc(double weight, double height)
    {
        Bundle extras = new Bundle();
        extras.putDouble("weight", weight);
        extras.putDouble("height", height);

        Intent intent = new Intent(MainActivity.this, CalcImc.class);
        intent.putExtras(extras);
        startActivity(intent);
    }
}